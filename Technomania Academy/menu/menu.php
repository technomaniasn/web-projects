<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../Mes_sites_tracker/cours/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="../Mes_sites_tracker/cours/ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="../Mes_sites_tracker/cours/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="../style/menu.css">
<link rel="stylesheet" href="../Mes_sites_tracker/cours/cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../Mes_sites_tracker/cours/cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">
  /* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
} 
</script>

<div class="topnav text-center" id="myTopnav">
      <a class="active" href="../../"><b>TechnoMania Academy</b></a>   
      <a href="../../user_dashboard.php">Tableau de bord</a>
      <a href="../../Documentations">Documentations</a>
      <a href="../../Lectures">Lectures</a>
      <a href="../../Videos">Videos</a>
      <a href="../../Partage">Partage de fichiers</a>
      <a href="../../Tutoriels">Tutoriels</a>
      <a href="../../Forum">Forum</a>
      <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        Menu
      </a>
</div>