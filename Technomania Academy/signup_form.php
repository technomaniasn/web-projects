<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="style/homesignup.css">
</head>
<body>

	<!-- The Modal -->
<div id="id01" class="modal">
  <!--<span onclick="document.getElementById('id01').style.display='block'" class="close" title="Close Modal">&times;</span>-->
 <!-- Button to open the modal -->
 <form class="modal-content animate" action="login_processor.php" style="border:1px solid #ccc">
  <div class="container">
    <h1>Inscription</h1>
    <p>Veuillez remplir ce formulaire pour vous inscrire.</p>
    <hr>

    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Entrer votre Email" name="email" required="true">

    <label for="psw"><b>Mot de passe</b></label>
    <input type="password" placeholder="Entrer votre mot de passe" name="psw" required="true">

    <label for="psw-repeat"><b>Confirmer le mot de passe</b></label>
    <input type="password" placeholder="Confirmer votre mot de passe" name="psw-repeat" required="true">

    <label>
      <input type="checkbox" name="remember" style="margin-bottom:15px"> Se rappeler de moi
    </label>

    <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

    <div class="clearfix">
      <a class="cancelBtn" href="index.php"><button type="button" class="cancelbtn">Fermer</button></a>
      <button type="submit" class="signupbtn">Valider</button>
    </div>
  </div>
</form> 
</div> 
</body>
</html>