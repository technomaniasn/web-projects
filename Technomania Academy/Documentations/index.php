<?php
session_start();
include "../menu/menu.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../style/documentations.css">
</head>
<body>
	<!-- <img class="mySlides" src="../images/app.jpg" style="width:100%"> -->
	<img class="mySlides" src="../images/mac.jpg" style="width:100%">
<!-- 	<img class="mySlides" src="../images/tech_drone.jpg" style="width:100%"> -->
	
	<div class="container jumbotron text-center" style="width:100%;">
		<h1>informatique</h1><br />
		<div class="row">
		<a target="_blank" href="../docsets/Apache_HTTP_Server.docset/Contents/Resources/Documents/httpd-docs/">
		  <div class="col-xs-6 col-sm-3">
			<img src="../images/icones/apache.png" style="width: 100px; margin: 20px;">
		  </div>
		</a>
		<a target="_blank" href="../docsets/Bash.docset/Contents/Resources/Documents/bash">
		  <div class="col-xs-6 col-sm-3">
		  	<img src="../images/icones/gnu-bash.png" style="width: 100px; margin: 20px;">
		  	<!-- <p>Bash</p> -->
	  	  </div>
	  	</a>
	  	<a target="_blank" href="../docsets/Bootstrap_3.docset/Contents/Resources/Documents/getbootstrap.com/">
	  		<div class="col-xs-6 col-sm-3">
	  			<img src="../images/icones/Boostrap.png" style="width: 100px; margin: 20px;">
	  			<!-- <p>Bootstrap 3</p> -->
	  		</div>
		</a>
		<a target="_blank" href="../docsets/CSS.docset/Contents/Resources/Documents/">
	  		<div class="col-xs-6 col-sm-3">
	  			<img src="../images/icones/css3.png" style="width: 100px; margin: 20px;">
	  			<!-- <p>CSS</p> -->
	  		</div>
	  	</a>
	  </div>
	  <div class="row">
	  <a target="_blank" href="../docsets/Django.docset/Contents/Resources/Documents/doc">	
	  <div class="col-xs-6 col-sm-3">
	  	<img src="../images/icones/django.png" style="width: 100px; margin: 20px;">
	  	<!-- <p>
	  		Django
	  	</p> -->
	  </div>
	  </a>
	  <a target="_blank" href="../docsets/Python_3.docset/Contents/Resources/Documents/doc">
	  <div class="col-xs-6 col-sm-3">
	  	<img src="../images/icones/python3.png" style="width: 100px; margin: 20px;">
	  	<!-- <p>
	  		Python 3
	  	</p> -->
	  </div>
	  </a>
	  <a target="_blank" href="../docsets/HTML.docset/Contents/Resources/Documents/">
	  <div class="col-xs-6 col-sm-3">
	  	<img src="../images/icones/html.png" style="width: 100px; margin: 20px;">
	  	<!-- <p>
	  		HTML
	  	</p> -->
	  </div>
	  </a>
	  <a target="_blank" href="../docsets/Docker.docset/Contents/Resources/Documents/docs.docker.com.html">
	  <div class="col-xs-6 col-sm-3">
	  	<img src="../images/icones/docker.png" style="width: 100px; margin: 20px;">
	  	<!-- <p>
	  		Docker
	  	</p> -->
	  </div>
	  </a>
	</div>
	<div class="row">
	  <a target="_blank" href="../Mes_sites_tracker/cours/www.w3schools.com/">
	  <div class="col-xs-6 col-sm-3">
	  	<img src="../images/icones/w3schools.jpg" style="width: 100px; margin: 20px;">
	  	<!-- <p>
	  		W3 Schools
	  	</p> -->
	  </div>
	  </a>
	  <a target="_blank" href="../docsets/PHP.docset/Contents/Resources/Documents/">
	  <div class="col-xs-6 col-sm-3">
	  	<img src="../images/icones/icone_php.png" style="width: 100px; margin: 20px;">
	  	<!-- <p>
	  		W3 Schools
	  	</p> -->
	  </div>
	  </a>
	  <a target="_blank" href="../Mes_sites_tracker/cours/apprendre-python.com/index.html">
	  <div class="col-xs-6 col-sm-3">
	  	<img src="../images/icones/apprendre-python.png" style="width: 100px; margin: 20px;">
	  	<!-- <p>
	  		W3 Schools
	  	</p> -->
	  </div>
	  </a>
	</div> 
	</div>
</body>
</html>