<?php
session_start();
include "../menu/menu.php";
include "../Database/db_connect.php";

$sql = "SELECT id, file_path, file_type, file_name, file_desc FROM uploaded WHERE file_type = 'pdf'";
$result = $conn->query($sql);

?>

<div class="jumbotron text-center">
  <h1>Vive la lecture !!</h1>
  <p>Des livres en versions PDF à dévorer sans modérations !</p>
</div>
<div class="container">

<?php
if ($result->num_rows > 0) {
    // output data of each row
    $tag = "<div class=\"row\" text-center>";
    $compteur = 0;
    while($row = $result->fetch_assoc()) {
  		echo $tag ."<a href=\"". $row['file_path'] . "\"><div class=\"col-xs-6 col-sm-3\" style=\"margin: 10%;\">
      		<h3>". $row["file_name"] . "</h3>
      		<div>" . $row['file_desc'] . "</div>
    	</div></a>";
    	// echo $compteur;
    	$compteur += 1;
    	if ($compteur == 3){
    		$compteur = 0;
    		$tag = "<div class=\"row\">";
    	}
    	else{
    		$tag = "";
    	} 
        // echo "id: " . $row["id"]. " - full path: " . $row["file_path"]. " " . "<br>";
    }
} else {
    echo "<h1 class=\"text-center\"> Huum où sont les livres ?? (: </h1> ";
}
$conn->close();
?>
</div>
    <!-- <div class="col-sm-4">
      <h3>Column 2</h3>
      <p>Lorem ipsum dolor..</p>
      <p>Ut enim ad..</p>
    </div>
    <div class="col-sm-4">
      <h3>Column 3</h3>
      <p>Lorem ipsum dolor..</p>
      <p>Ut enim ad..</p>
    </div>
  </div>
</div>  -->