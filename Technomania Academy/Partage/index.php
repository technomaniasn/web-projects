<?php
session_start();
include "../menu/menu.php";
?>
<head>
	<script type="text/javascript" src="test.js"></script>
</head>
<html>
<body class="text-center">

<div class="container jumbotron text-center" style="width: 100%; height: 100%;">
	<div class="row text-center">
	<div>
	   	<h1>Selectionner un fichier à partager avec la communauté:</h1>
	</div>
	<form action="files_uploader.php" method="POST" enctype="multipart/form-data">
	    <div class="col-sm-6 jumbotron text-center" style="margin-top: 50px;">
	    	<div>
	    		<label for="fileToUpload">*Choisir un pdf, docx, pptx, txt ou un mp4, webm</label>
	    		<input type="file" name="fileToUpload[]" id="fileToUpload" accept=".mp4, .webm, .pdf, .docx, .pptx, .txt" multiple>
	    	</div>
		</div>
		<div class="row">
		<div class="col-sm-12">
	    	<button type="submit" class="btn btn-success" style="margin: 20px;">Envoyer</button>
	   	</div>
	   </div>
	</form>
	</div>
</div>
</body>
</html>

<!-- accept=".pdf, .jpg, .png" -->