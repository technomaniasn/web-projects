<?php
/*
NB: Always change user group and execution right on the unix system for new directory to allows upload on them.
*/

//Start a session to handle all session environement
session_start();
$_SESSION['error_log'] = false;

$servername = "localhost";
$username = "root";
$password = "usbw";
$dbname = "tm_academy";

if ($_SESSION['error_log'] == true)
{
	echo "<script> alert (\"Une erreur est survenue durant l'envoi! Merci de ressayer !\")</script>";
}


//trying to catch all exception error
if ($_SERVER['REQUEST_METHOD'] == 'POST'){

	$i=0;
	while ($i < count($_FILES['fileToUpload']['name'])){
	try 
	{

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);

		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		// if (isset($_POST['filename']) and $_POST['filename'] != "" and isset($_POST['filedesc']) and $_POST['filedesc'] != "")
		// {
		$file_control = 0;
		if ($_FILES['fileToUpload']['error'][$i] == 0)
		{
			/*
				Getting posted name and type of file.
				Changing the temp_name attribute with posted name 
				Building a new full name and defining some variables
			*/
			$file_control += 1;
			// echo "" . "<br/>";
			// $file_post_name = $_POST['filename'];
			$file_post_name = $_FILES['fileToUpload']['name'][$i];
			$file_type = strtolower(pathinfo($_FILES["fileToUpload"]["name"][$i],PATHINFO_EXTENSION));
			$_FILES["fileToUpload"]["name"][$i] = $file_post_name . "." . $file_type;
			$file_name = $file_post_name;

			//////Making some checklist to verify if the file is responding to our needs///////////	

			//Check the type of the file to know where to put it (directory)
			// echo "Checking file type" . "<br/>";
			if($file_type == "mp4" or $file_type == "webm")
			{
				$upload_dir = "../Partage/movies/";
				// echo "setting upload directory to movies" . "<br/>";
				$file_control += 1;
			}
			elseif ($file_type == "pdf" or $file_type == "docx" or $file_type == "pptx" or $file_type == "txt") 
			{
				$upload_dir = "../Partage/pdf/";
				// echo "setting upload directory to pdf" . "<br/>";
				$file_control += 1;
			}
			else
			{
				// echo "File format not supported" . "<br/>";			
				$upload_dir = "";
				$_SESSION['error_log'] = true;
				// header("Location: ../Partage/");		
			}

			//Building complete path to the file
			$full_path = $upload_dir . $file_name;

			//Checking existence of file with same name
			if (file_exists($full_path))
			{
				$_SESSION['error_log'] = true;
			    // echo "File with the same name already exists in this location !!" . "<br/>";
			    
			}
			else
			{
				$file_control += 1;
				// echo "File is not already present here !!" . "<br/>";
			}
			// echo "Number of control passed == " . $file_control . "<br />";
			if($file_control == 3)
			{
				
				// echo "Uploading file to ..." . $full_path . "<br/>";
				// print_r($_FILES);
				move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $full_path);
				$_SESSION['full_path'] = $full_path;
				$_SESSION['file_type'] = $file_type;
				$_SESSION['file_name'] = $_POST['filename'];
				$_SESSION['file_desc'] = $_POST['filedesc'];
				$_SESSION['upload_succed'] = true;
				// echo "successed on upload" . "<br/>";

				// prepare and bind
				$stmt = $conn->prepare("INSERT INTO uploaded (file_path, file_type, file_name, file_desc) VALUES (?, ?, ?, ?)");
				$stmt->bind_param("ssss", $full_path, $file_type, $file_name, $file_desc);
				$stmt->execute();
				header("Location: ../Partage/");
			}
			else
			{
				$_SESSION['error_log'] = true;
				// echo "Upload abort" . "<br/>";
				header("Location: ../Partage/");
			}
		}
		else
		{
			//echo "<script> alert (\"file has not been submited\")</script>";
			$_SESSION['error_log'] = true;
			header("Location: ../Partage/");
			// echo "file has not been submited";
			
		}

		
		// // printing debugging informations to the page
		// echo $file_name . "<br/>" . $file_post_name . "<br/>" ; 
		// echo $full_path . "<img src=\"" . $full_path . "\"" . ">" ;
		// move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $full_path);
		// header("Location: ../Database/db_file_register.php");
		// print_r($_FILES);
			
		// }
		// else
		// {
		// 	$_SESSION['error_log'] = true;
		// 	echo "no data";
			
		// }
		// header("Location: ../Database/db_file_register.php");
	}
	//catch exception
	catch(Exception $e) 
	{
	  echo 'Message: ' .$e->getMessage();
	}
	 $i++;
	}
}
?>
