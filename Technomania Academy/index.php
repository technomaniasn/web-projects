<?php
  session_start();
?>

<html lang="fr">
<head>
  <title>Technomania Academy</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</head>
<body>

<div class="jumbotron text-center">
  <h1>TechnoMania Academy</h1>
  <h4><b>La bibliothèque numérique hors-ligne<br/> pour apprendre sans internet</b></h4> 
</div>
  
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <p>
        <a href="signup_form.php" class="btn btn-warning" style="width: 100%; font-size: 20px;">S'inscrire</a>
      </p>
    </div>
    <div class="col-sm-6">
      <p>
        <a href="signin_form.php" class="btn btn-success" style="width: 100%; font-size: 20px;">Se connecter</a>
      </p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <h3>
        Apprendre les métiers du numérique dans des endroits réculés n'ayant pas un accès constant à internet est un défis à relever. 
      </h3>
      <p>
        Avec <b>TechnoMania Academy</b> nous comptons apporter un début de solution à ce challenge.
        Inspirer par l'incroyable projet de <a href="#">bibliothèque sans frontières</a>, le <strong>KoomBook</strong>.
        <b>TechnoMania Academy</b> met à la disposition des établissements scolaires une bibliothèque numérique contenant
        des sites web et vidéos éducatives consultables en mode hors-ligne depuis un smartphone, tablette et pc.
      </p>
    </div>
  </div>
</div>

</body>
</html>