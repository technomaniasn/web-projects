<!DOCTYPE html>
<html>
<head>
  <title>login</title>
  <link rel="stylesheet" type="text/css" href="style/homesignin.css">
</head>
<body>



<!-- The Modal -->
<div id="id01" class="modal">
  <!--
  <span onclick="document.getElementById('id01').style.display='block'"
class="close" title="Close Modal">&times;</span>-->

  <!-- Modal Content -->
  <form class="modal-content animate" action="login_processor.php" method="POST">
    <div class="imgcontainer">
      <img src="images/logoTechno.jpg" alt="Avatar" class="avatar">
    </div>

    <div class="container">
      <label for="uname"><b>Utilisateur</b></label>
      <input type="text" placeholder="Entrer votre nom d'utilisateur ou email" name="uname" required="true">

      <label for="psw"><b>Mot de passe</b></label>
      <input type="password" placeholder="Entrer votre mot de passe" name="psw" required="true">

      <button type="submit">Valider</button>
      <label>
        <input type="checkbox" name="remember"> Se rappeler de moi
      </label>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <a class="cancelBtn" href="index.php">
        <button type="button" class="cancelbtn">Fermer</button>
      </a>
      <span class="psw"><a href="#">Mot de passe oublier ?</a></span>
    </div>
  </form>
</div>  
</body>
</html>